/*
TAREA: Empezar preguntando cuánta gente hay en el grupo familiar.
Crear tantos inputs+labels como gente haya para completar la edad de cada integrante.
Al hacer click en "calcular", mostrar en un elemento pre-existente la mayor edad, la menor edad y el promedio del grupo familiar.

Punto bonus: Crear un botón para "empezar de nuevo" que empiece el proceso nuevamente, borrando los inputs ya creados (investigar cómo en MDN).
*/

document.querySelector("#boton-siguiente").onclick = function () {
  const cantidadIntegrantes = Number(
    document.querySelector("#cantidad-integrantes").value
  );
  borrarIntegrantes();
  crearIntegrantes(cantidadIntegrantes);

  return false;
};




function crearIntegrante(indice) {
  let $div = document.createElement("div");
  $div.className = "integrante";
  
  const nuevosIntegrantes = document.createElement("input");
  nuevosIntegrantes.type = "number";
  const textoIntegrantes = document.createTextNode(
    "Nota del parcial " + (indice + 1)
    );
    
    $div.appendChild(textoIntegrantes);
    $div.appendChild(nuevosIntegrantes);
    const nodoIntegrante = document.querySelector("#integrantes");
    nodoIntegrante.appendChild($div);
  }
  
  function crearIntegrantes(cantidadIntegrantes) {
    if (cantidadIntegrantes > 0) {
      mostrarBotonCalculo();
    }
  
    for (let i = 0; i < cantidadIntegrantes; i++) {
      crearIntegrante(i);
    }
  }

function borrarIntegrantes() {
  const $integrantes = document.querySelectorAll(".integrante");
  for (let i = 0; i < $integrantes.length; i++) {
    $integrantes[i].remove();
  }
}
function mostrarResultados(){
  document.querySelector('#resultados').className = '';
}

function ocultarResultados(){
    document.querySelector('#resultados').className = 'oculto'
}

document.querySelector("#resetear").onclick = function resetear() {
  borrarIntegrantes();
  ocultarResultados()
};

function mostrarBotonCalculo() {
  document.querySelector("#boton-calcular").className = "";
}


document.querySelector("#boton-calcular").onclick = function () {
  const numeros = obtenerEdadesIntegrantes();
  mostrarEdad('mayor', calculaMayor(numeros));
  mostrarEdad('menor', calculaMenor(numeros));
  mostrarEdad('promedio', calculaPromedio(numeros));
  mostrarResultados()

};

function obtenerEdadesIntegrantes() {
  let $integrantes = document.querySelectorAll(".integrante input");
  let edades = [];

  for (let i = 0; i < $integrantes.length; i++) {
    edades.push(Number($integrantes[i].value));
  }
  return edades;
}

function mostrarEdad(tipo, valor) {
    document.querySelector(`#${tipo}-edad`).textContent = valor
}


/*
TAREA:
Crear una interfaz que permita agregar ó quitar (botones agregar y quitar) inputs+labels para completar el salario anual de cada integrante de la familia que trabaje.
Al hacer click en "calcular", mostrar en un elemento pre-existente el mayor salario anual, menor salario anual, salario anual promedio y salario mensual promedio.

Punto bonus: si hay inputs vacíos, ignorarlos en el cálculo (no contarlos como 0).
*/
