
function calculaMayor(numeros) {
  let mayorNumero = numeros[0];

  for (let i = 1; i < numeros.length; i++) {
    if (numeros[i] > mayorNumero)
      mayorNumero = numeros[i];
  }
  return mayorNumero;
}

function calculaMenor(numeros) {
  let menorNumero = numeros[0];
  for (let i = 1; i < numeros.length; i++) {
    if (numeros[i] < menorNumero)
      menorNumero = numeros[i];
  }
  return menorNumero;
}

function calculaPromedio(numeros) {
  let suma = 0;
  for (let i = 0; i < numeros.length; i++) {
    suma += numeros[i];
  }
  let promedio = Math.floor(suma / numeros.length)
  return promedio
}
