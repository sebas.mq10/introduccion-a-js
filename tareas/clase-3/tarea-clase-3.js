// Tarea 1:
// Preguntarle al usuario su nombre.
// Si el nombre del usuario es el mismo que  el  de ustedes
// Imprimir "Hola, Tocayo! Yo también me llamo " y su nombre.
// Elijan otro nombre, puede ser de un pariente, amigo, conocido.
// Si el nombe del usuario es el mismo que el que nombre que eligieron
// Imprimir "Hola " y el nombre, " te llamás igual que mi ..."
// Si no, simplemente imprimir "Hola " + nombre!

/*

const nombreUsuario = prompt("Hola, como te llamas?").toLowerCase();
function saludarUsuario (){
    switch(nombreUsuario){
        case 'sebastian':
            alert("Hola Tocayo!");
            break;
        case 'camila':
            alert("Mi novia se llama igual!");
            break;
        default:
            alert("hola " + nombreUsuario)
    }   
}

saludarUsuario()

*/

//Tarea 2:
// Preguntar la edad del usuario
// Hacerle saber si tiene más, menos ó la misma edad que nosotros.

/*
const edadUsuario = Number(prompt("Que edad tenes?"))

if(edadUsuario === 32){
    alert("Tenemos la misma edad!")
}else if(edadUsuario > 32){
    alert("Sos mas grande que yo")
}else{
    alert("Sos mas chico que yo")
}

*/

//Tarea 3:
// Preguntarle al usuario si tiene documento, y que conteste con "si" o "no".
// Si dice si, preguntarle la edad.
// Si la edad es mayor a 18, dejarlo entrar al bar.
// Si la edad es menor a 18, no dejarlo entrar al bar.
// Si no tiene documento, no dejarlo entrar al bar.
// Si no entendemos la respuesta, le decimos que no entendimos la respuesta.
// Punto bonus: SI, NO, Si, No, si, no.

const EDAD_MINIMA_PARA_ENTRAR = 18;
const RESPUESTA_SI = 'si';
const RESPUESTA_NO = 'no';

const tieneDocumento = prompt("Tenes documento?").toLowerCase;

if(tieneDocumento === RESPUESTA_SI){
    const edadUsuario = Number(prompt("Cuantos años tenes?"));

    if(edadUsuario >= EDAD_MINIMA_PARA_ENTRAR){
        alert("Bienvenido al bar")
    }else if(edadUsuario < EDAD_MINIMA_PARA_ENTRAR){
        alert("No podes entrar, sos muy chiquite")
    }else{
        alert("No entendi la respuesta")
    }

} else if(tieneDocumento === RESPUESTA_NO){
    alert("Necesitas documento para ingresar al bar")
} else{
    alert("No entendi si tenes o no dni")
}