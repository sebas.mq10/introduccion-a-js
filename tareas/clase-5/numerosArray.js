//TAREA: En otro archivo distinto,
// Crear una lista de <ol> y <li> que contengan sólo números.
// Convertir esos números a un array y:
// 1. calcular el promedio y mostrarlo en un <em> pre-creado con el texto "El promedio es..."
// 2. obtener el número más pequeño y mostrarlo en un <em> pre-creado con el texto "El número más pequeño es..."
// 3. obtener el número más grande y mostrarlo en un <em> pre-creado con el texto "El número más grande es..."
// 4. obtener el número que más se repite y mostrarlo en un <em> pre-creado con el texto "El número más frecuente es..."

let $numeros = document.getElementById("lista").getElementsByTagName("li");

let arrayDeNumeros = [];

function convertirAArray() {
  for (let i = 0; i < $numeros.length; i++) {
    arrayDeNumeros.push($numeros[i].innerText);
  }
  arrayDeNumeros = arrayDeNumeros.map(Number);
  return arrayDeNumeros;
}
convertirAArray();

function calculaPromedio() {
  let total = 0;
  for (let i = 0; i < $numeros.length; i++) {
    total += arrayDeNumeros[i];
  }
  return Math.floor(total / arrayDeNumeros.length);
}

function calculaMasPequenio() {
  let masPequenio = arrayDeNumeros[0];
  for (let i = 0; i < arrayDeNumeros.length; i++) {
    if (arrayDeNumeros[i] < masPequenio) masPequenio = arrayDeNumeros[i];
  }
  return masPequenio;
}

function calculaMasGrande() {
  let masGrande = arrayDeNumeros[0];
  for (let i = 0; i < arrayDeNumeros.length; i++) {
    if (arrayDeNumeros[i] > masGrande) masGrande = arrayDeNumeros[i];
  }
  return masGrande;
}

function calculaMasFrecuente() {
  let mapeo = {};
  let masFrecuente = arrayDeNumeros[0],
    recuento = 1;
  for (let i = 0; i < arrayDeNumeros.length; i++) {
    let elemento = arrayDeNumeros[i];
    if (mapeo[elemento] == null) mapeo[elemento] = 1;
    else mapeo[elemento]++;
    if (mapeo[elemento] > recuento) {
      masFrecuente = elemento;
      recuento = mapeo[elemento];
    }
  }
  return masFrecuente;
}



document.querySelector("#calcular").onclick = function (){

document.querySelector("#promedio").innerText = calculaPromedio();
document.querySelector("#mas-pequenio").innerText = calculaMasPequenio();
document.querySelector("#mas-grande").innerText = calculaMasGrande();
document.querySelector("#mas-frecuente").innerText = calculaMasFrecuente();

}