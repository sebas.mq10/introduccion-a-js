const {
  imprimirNumeros,
  celciusAFahrenheit,
  promedio,
  mostrarPositivos,
  mostrarMasGrande,
  fibonacciSinRecursion,
} = require("./tarea4-algoritmos");

const realConsole = global.console;
const consoleLogFalso = jest.fn();

describe.skip("imprimirNumeros", () => {
  beforeEach(() => {
    global.console = {
      ...realConsole,
      log: consoleLogFalso,
    };
  });
  afterEach(() => {
    global.console = realConsole;
  });

  test("debe imprimir 10 veces a consola ", () => {
    imprimirNumeros();

    expect(consoleLogFalso).toHaveBeenCalledTimes(10);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(1, 1);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(2, 2);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(3, 3);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(4, 4);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(5, 5);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(6, 6);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(7, 7);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(8, 8);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(9, 9);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(10, 10);
  });
});
describe.skip("Conversor de celcius a fahrenheit", () => {
  test.each([
    [100, 212],
    [0, 32],
    [50, 122],
  ])("chequea conversion correcta %p -> %p", (celsius, fahrenheit) => {
    expect(celciusAFahrenheit(celsius)).toStrictEqual(fahrenheit);
  });
});

describe.skip("Calcula el promedio de todos los nros en un array", () => {
  test.each([
    [[2, 6, 8, 4], 5],
    [[10, 20, 30, 40, 10], 22],
  ])("chequea que haga el promedio ", (nrosPrueba, ResultadoPromedio) => {
    expect(promedio(nrosPrueba)).toStrictEqual(ResultadoPromedio);
  });
});

describe.skip("Devuelve solo los numeros positivos de un array", () => {
  test.each([
    [
      [-1, 2, -3, 4],
      [2, 4],
    ],
  ])(
    "chequea que pase solo los positivos",
    (numerosArray, resultadoPositivos) => {
      expect(mostrarPositivos(numerosArray)).toStrictEqual(resultadoPositivos);
    }
  );

  test("si le pasas solo negativos no se puede hacer", () => {
    expect(mostrarPositivos([-1, -2, -3])).toThrowError;
  });
});

describe.skip("Debe buscar el nro maximo de un array", () => {
  test.each([
    [[1, 2, 3, 4], 4]
  ])(
    "chequea que busque el mas grande",
    (nrosRandom, nroMasGrande) => {
      expect(mostrarMasGrande(nrosRandom)).toStrictEqual(nroMasGrande);
    }
  );
});

describe("console.log de nros de fibonacci", () => {
  beforeEach(() => {
    global.console = {
      ...realConsole,
      log: consoleLogFalso,
    };
  });
  afterEach(() => {
    global.console = realConsole;
  });

  test("debe imprimir los primeros 10 digitos de Fibonacci sin recursion", () => {
    fibonacciSinRecursion();

    expect(consoleLogFalso).toHaveBeenCalledTimes(10);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(1, 0);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(2, 1);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(3, 1);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(4, 2);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(5, 3);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(6, 5);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(7, 8);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(8, 13);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(9, 21);
    expect(consoleLogFalso).toHaveBeenNthCalledWith(10, 34);
  });
});
