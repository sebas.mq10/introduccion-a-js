const {
    esPrimo,
}= require("./esPrimo");




describe("", () => {
    test.each([
      [2, true],
      [3, true],
      [4, false],
    ])("Debe retornar un boolean dependiendo si es primo o no %p -> %p", (nroRandom, resultado) => {
      expect(esPrimo(nroRandom)).toStrictEqual(resultado);
    });

    test("0 debe decir que no es primo",()=>{
        expect(esPrimo(0)).toBe(`no es primo`)
    });
    test("1 debe decir que no es primo ni compuesto",()=>{
        expect(esPrimo(1)).toBe(`no es ni primo ni compuesto`)
    })
    test("nro negativo debe dar error",()=>{
        expect(esPrimo(-1)).toThrowError
    })
  });