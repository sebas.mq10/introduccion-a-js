function imprimirNumeros() {
  for (i = 1; i <= 10; i++) {
    console.log(i);
  }
}
function celciusAFahrenheit(n) {
  return n * 1.8 + 32;
}

// const miArray = [1, 3, 7, 9, 10];
// let suma = 0;

// function promedio() {
//   for (i = 0; i < miArray.length; i++) {
//     suma = suma + miArray[i];
//   }
//   const total = suma / miArray.length;
//   return total;
// }

function promedio(n) {
  let suma = 0;

  for (i = 0; i < n.length; i++) {
    suma = suma + n[i];
  }
  return suma / n.length;
}

// let listaDeArrays = [-1, 2, -15, 20, -5, 34];

// function mostrarPositivos() {
//   let arrayPositivo = [];
//   for (i = 0; i < listaDeArrays.length; i++) {
//     if (listaDeArrays[i] >= 0) arrayPositivo.push(listaDeArrays[i]);
//   }
//   return arrayPositivo;
// }

function mostrarPositivos(n) {
  let arrayPositivo = [];
  for (i = 0; i < n.length; i++) {
    if (n[i] >= 0) {
      arrayPositivo.push(n[i]);
    }
  }
  return arrayPositivo;
}

// let ar = [1, 2, 3, 20];

// function mostrarMasGrande(ar) {
//   let max = ar[0];
//   for (i = 0; i < ar.length; i++) {
//     if (ar[i] > max) {
//       max = ar[i];
//     }
//   }
//   return max;
// }
// let max = mostrarMasGrande(ar);

function mostrarMasGrande(x) {
  let numeroMasGrande = x[0];
  for (i = 0; i < x.length; i++) {
    if (x[i] > numeroMasGrande) {
      numeroMasGrande = x[i];
    }
  }
  return numeroMasGrande;
}

function fibonacciSinRecursion() {
  let n1 = Number(0);
  let n2 = Number(1);
  let sigNum;

  for (let i = 1; i <= 10; i++) {
    console.log(n1);
    sigNum = n1 + n2;
    n1 = n2;
    n2 = sigNum;
  }
}

module.exports = {
  imprimirNumeros,
  celciusAFahrenheit,
  promedio,
  mostrarPositivos,
  mostrarMasGrande,
  fibonacciSinRecursion,
};
