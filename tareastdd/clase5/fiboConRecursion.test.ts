const { fibonacciConRecursion } = require("./fiboConRecursion");

describe("debe encontrar el nro n de fibonacci con recursion ", () => {
  test.each([
    [0,0],
    [1,1],
    [2,1],
    [3,2],
    [4,3],
    [5,5],
    [6,8],
    [7,13],
    [8,21],
    [9,34],
    [10,55],
  ])("cuando n sea %p tiene que devolver %p", (numero, devolucion) => {
    expect(fibonacciConRecursion(numero)).toStrictEqual(devolucion);
  });
});
