function fibonacciConRecursion(n) {
  if (n < 2) {
    return n;
  }
  return fibonacciConRecursion(n - 1) + fibonacciConRecursion(n - 2);
}


module.exports = {
  fibonacciConRecursion,
};
